FROM python:3.7-alpine3.9

ENV SCREEN_WIDTH 1280
ENV SCREEN_HEIGHT 720
ENV SCREEN_DEPTH 16

RUN apk update;
RUN apk add --no-cache chromium chromium-chromedriver udev xvfb firefox-esr ttf-freefont;

RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip install -r requirements.txt

RUN sed -i "s/self._arguments\ =\ \[\]/self._arguments\ =\ \['\--headless'\,'\--no-sandbox'\,\'--disable-gpu'\,'\--disable-dev-shm-usage'\]/" $(python -c "import site; print(site.getsitepackages()[0])")/selenium/webdriver/chrome/options.py;





